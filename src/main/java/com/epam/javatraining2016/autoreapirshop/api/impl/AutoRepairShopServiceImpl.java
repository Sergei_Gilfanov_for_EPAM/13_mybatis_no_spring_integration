package com.epam.javatraining2016.autoreapirshop.api.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.dao.AutoRepairShopServiceDao;
import com.epam.javatraining2016.autoreapirshop.dao.ClientRow;
import com.epam.javatraining2016.autoreapirshop.dao.CommentRow;
import com.epam.javatraining2016.autoreapirshop.dao.InvoiceListRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRecordRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderHistoryRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderListRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderRow;
import com.epam.javatraining2016.autoreapirshop.dao.OrderStatusRow;
import com.epam.javatraining2016.autoreapirshop.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.protocol.Order;

@Component(value = "autoRepairShopService")
public class AutoRepairShopServiceImpl implements AutoRepairShopService {
  private static final Logger log = LoggerFactory.getLogger(AutoRepairShopServiceImpl.class);

  @Autowired
  AutoRepairShopServiceDao dao;

  public AutoRepairShopServiceImpl() {
    log.info("AutoRepairShopServiceImpl:default constructor");
  }

  @Override
  public String getVersion() {
    return "sergei_gilfanov@epam.com,2016-06-27";
  }

  @Override
  @Transactional
  public int createClient(String name) {
    log.info("createClient");
    ClientRow retval;
    OrderListRow orderList = dao.orderList().create();
    retval = dao.client().create(name, orderList);
    return retval.getId();
  }

  @Override
  @Transactional
  public Client getClient(int clientId) {
    log.info("getClient");
    ClientRow client;
    client = dao.client().getShallow(clientId);

    Client retval = new Client();
    retval.setId(client.getId());
    retval.setClientName(client.getClientName());
    retval.setOrderListId(client.getOrderList().getId());
    return retval;
  }

  @Override
  @Transactional
  public int createComment(int orderId, String commentText) {
    log.info("createComment");
    OrderHistoryRecordRow orderHistoryRecord;
    OrderRow order = dao.order().getShallow(orderId);
    CommentRow comment = dao.comment().create(commentText);
    orderHistoryRecord = dao.orderHistoryRecordComment().create(order.getOrderHistory(), comment);
    return orderHistoryRecord.getId();
  }

  @Override
  @Transactional
  public Comment getComment(int commentId) {
    log.info("getComment");
    CommentRow comment;
    comment = dao.comment().get(commentId);
    Comment retval = new Comment();
    retval.setId(comment.getId());
    retval.setCommentText(comment.getCommentText());
    return retval;
  }

  @Override
  @Transactional
  public int createOrder(int clientId) {
    log.info("createOrder");
    OrderRow order;
    ClientRow client = dao.client().getShallow(clientId);
    InvoiceListRow invoiceList = dao.invoiceList().create();
    OrderHistoryRow orderHistory = dao.orderHistory().create();
    order = dao.order().create(invoiceList, orderHistory);
    dao.orderList().add(client.getOrderList(), order);
    changeOrderStatusCommon(dao, order, "new");
    return order.getId();
  }

  @Override
  @Transactional
  public Order getOrder(int orderId) {
    log.info("getComment");
    OrderRow order;
    OrderStatusRow status;
    order = dao.order().getShallow(orderId);
    status = dao.orderStatus().getCurrentStatus(order);

    Order retval = new Order();
    retval.setId(order.getId());
    retval.setStatus(status.getStatusName());
    return retval;
  }

  public OrderHistoryRecordRow changeOrderStatusCommon(AutoRepairShopServiceDao dao, OrderRow order,
      String statusName) {
    OrderStatusRow status = dao.orderStatus().findByName(statusName);
    CommentRow comment = dao.comment().create(String.format("Status changed to %s", statusName));
    OrderHistoryRecordRow orderHistoryRecord =
        dao.orderHistoryRecordStatusChange().create(order.getOrderHistory(), status, comment);
    return orderHistoryRecord;
  }

  @Override
  @Transactional
  public int changeOrderStatus(int orderId, String statusName) {
    OrderHistoryRecordRow orderHistoryRecord;
    OrderRow order = dao.order().getShallow(orderId);
    orderHistoryRecord = changeOrderStatusCommon(dao, order, statusName);
    return orderHistoryRecord.getId();
  }

  @Override
  @Transactional
  public Order[] getOrdersForClient(int clientId) {
    List<Order> retval;
    ClientRow client = dao.client().getShallow(clientId);
    List<OrderRow> orders = dao.order().readList(client.getOrderList());
    retval = new ArrayList<Order>(orders.size());
    for (OrderRow orderRow : orders) {
      Order order = new Order();
      OrderStatusRow orderStatus = dao.orderStatus().getCurrentStatus(orderRow);
      order.setId(orderRow.getId());
      order.setStatus(orderStatus.getStatusName());
      retval.add(order);
    }
    return retval.toArray(new Order[0]);
  }
}
