package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class OrderDao {
  @Autowired
  private OrderMapper mapper;

  public OrderRow create(InvoiceListRow invoiceList, OrderHistoryRow orderHistory) {
    OrderRow retval = new OrderRow(0);
    retval.setInvoiceList(invoiceList);
    retval.setOrderHistory(orderHistory);
    mapper.insert(retval);
    return retval;
  }

  @Cacheable("Order")
  public OrderRow getShallow(int orderId) {
    OrderRow retval = mapper.selectShallow(orderId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public List<OrderRow> readList(OrderListRow orderList) {
    List<OrderRow> retval = mapper.readList(orderList);
    return retval;
  }

}
