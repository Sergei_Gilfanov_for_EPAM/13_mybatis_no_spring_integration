package com.epam.javatraining2016.autoreapirshop.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AutoRepairShopServiceDao {
  // private static final Logger log = LoggerFactory.getLogger(AutoRepairShopServiceDao.class);

  @Autowired
  private OrderHistoryDao orderHistoryDao;

  public OrderHistoryDao orderHistory() {
    return orderHistoryDao;
  }

  @Autowired
  private InvoiceListDao invoiceListDao;

  public InvoiceListDao invoiceList() {
    return invoiceListDao;
  }

  @Autowired
  private OrderHistoryRecordDao orderHistoryRecordDao;

  public OrderHistoryRecordDao orderHistoryRecord() {
    return orderHistoryRecordDao;
  }

  @Autowired
  private OrderListDao orderListDao;

  public OrderListDao orderList() {
    return orderListDao;
  }

  @Autowired
  private OrderDao orderDao;

  public OrderDao order() {
    return orderDao;
  }

  @Autowired
  private OrderStatusDao orderStatusDao;

  public OrderStatusDao orderStatus() {
    return orderStatusDao;
  }

  @Autowired
  private CommentDao commentDao;

  public CommentDao comment() {
    return commentDao;
  }

  @Autowired
  private ClientDao clientDao;

  public ClientDao client() {
    return clientDao;
  }

  @Autowired
  private OrderHistoryRecordStatusChangeDao orderHistoryRecordStatusChangeDao;

  public OrderHistoryRecordStatusChangeDao orderHistoryRecordStatusChange() {
    return orderHistoryRecordStatusChangeDao;
  }

  @Autowired
  private OrderHistoryRecordCommentDao orderHistoryRecordCommentDao;

  public OrderHistoryRecordCommentDao orderHistoryRecordComment() {
    return orderHistoryRecordCommentDao;
  }
}
