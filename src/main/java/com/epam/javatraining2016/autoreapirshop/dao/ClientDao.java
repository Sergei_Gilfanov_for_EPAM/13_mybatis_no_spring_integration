package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class ClientDao {
  @Autowired
  private ClientMapper mapper;

  public ClientRow create(String name, OrderListRow orderList) {
    ClientRow retval = new ClientRow(0);
    retval.setClientName(name);
    retval.setOrderList(orderList);
    mapper.insert(retval);
    return retval;
  }

  @Cacheable("Client")
  public ClientRow getShallow(int clientId) {
    ClientRow retval = mapper.selectShallow(clientId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
