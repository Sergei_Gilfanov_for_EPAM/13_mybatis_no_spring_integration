package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InvoiceListDao {
  @Autowired
  private InvoiceListMapper mapper;

  public InvoiceListRow create() {
    InvoiceListRow retval = new InvoiceListRow(0);
    mapper.insert(retval);
    return retval;
  }

  public InvoiceListRow get(int invoiceListId) {
    InvoiceListRow retval = mapper.select(invoiceListId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
