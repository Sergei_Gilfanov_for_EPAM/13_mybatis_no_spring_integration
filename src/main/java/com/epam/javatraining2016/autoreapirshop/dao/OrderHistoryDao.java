package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderHistoryDao {
  @Autowired
  private OrderHistoryMapper mapper;

  public OrderHistoryRow create() {
    OrderHistoryRow retval = new OrderHistoryRow(0);
    mapper.insert(retval);
    return retval;
  }

  public OrderHistoryRow get(int orderHistoryId) {
    OrderHistoryRow retval = mapper.select(orderHistoryId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
