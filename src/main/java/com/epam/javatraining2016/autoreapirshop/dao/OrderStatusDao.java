package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class OrderStatusDao {
  @Autowired
  private OrderStatusMapper mapper;

  public OrderStatusRow get(int orderStatusId) {
    OrderStatusRow retval = mapper.select(orderStatusId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  @Cacheable("OrderStatus")
  public OrderStatusRow findByName(String statusName) {
    OrderStatusRow retval = mapper.selectByName(statusName);
    return retval;
  }

  public OrderStatusRow getCurrentStatus(OrderRow order) {
    OrderStatusRow retval = mapper.selectCurrentStatus(order.getOrderHistory());
    if (retval == null) {
      // Не должно происходит - мы при создании заказа создаем одну запись истории
      throw new NoSuchElementException();
    }
    return retval;
  }
}
