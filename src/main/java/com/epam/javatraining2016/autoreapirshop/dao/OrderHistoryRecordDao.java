package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderHistoryRecordDao {
  @Autowired
  OrderHistoryRecordMapper mapper;

  public OrderHistoryRecordRow create(OrderHistoryRow orderHistory) {
    OrderHistoryRecordRow retval = new OrderHistoryRecordRow(0);
    retval.setOrderHistory(orderHistory);
    mapper.insert(retval);
    return retval;
  }

  public OrderHistoryRecordRow getShallow(int orderHitoryRecordId) {
    OrderHistoryRecordRow retval = mapper.selectShallow(orderHitoryRecordId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
