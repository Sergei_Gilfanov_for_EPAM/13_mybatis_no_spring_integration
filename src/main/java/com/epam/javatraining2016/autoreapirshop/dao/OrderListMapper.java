package com.epam.javatraining2016.autoreapirshop.dao;

import org.apache.ibatis.annotations.Param;

public interface OrderListMapper {
  void insert(OrderListRow orderList);

  OrderListRow select(int orderListId);

  void add(@Param("orderList") OrderListRow orderList, @Param("order") OrderRow order);
}
