package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class OrderHistoryRecordCommentDao {
  @Autowired
  private OrderHistoryRecordMapper parentMapper;
  @Autowired
  private OrderHistoryRecordCommentMapper mapper;

  public OrderHistoryRecordCommentRow create(OrderHistoryRow orderHistory, CommentRow comment) {
    OrderHistoryRecordCommentRow retval = new OrderHistoryRecordCommentRow(0);
    retval.setOrderHistory(orderHistory); // Parent (OrderHistoryRecord) property
    retval.setComment(comment);

    parentMapper.insert(retval);
    mapper.insert(retval);
    return retval;
  }

  @Cacheable("OrderHistoryComment")
  public OrderHistoryRecordCommentRow getShallow(int recordId) {
    OrderHistoryRecordCommentRow retval = mapper.selectShallow(recordId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
