package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class OrderListDao {
  @Autowired
  private OrderListMapper mapper;

  public OrderListRow create() {
    OrderListRow retval = new OrderListRow(0);
    mapper.insert(retval);
    return retval;
  }

  public OrderListRow get(int orderListId) {
    OrderListRow retval = mapper.select(orderListId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public void add(OrderListRow orderList, OrderRow order) {
    mapper.add(orderList, order);
  }
}
