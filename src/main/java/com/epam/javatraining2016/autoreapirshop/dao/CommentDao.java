package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDao {
  @Autowired
  private CommentMapper mapper;

  public CommentRow create(String commentText) {
    CommentRow retval = new CommentRow(0);
    retval.setCommentText(commentText);
    mapper.insert(retval);
    return retval;
  }

  public CommentRow get(int commentId) {
    CommentRow retval = mapper.select(commentId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
