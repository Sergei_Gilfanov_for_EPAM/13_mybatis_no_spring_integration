package com.epam.javatraining2016.autoreapirshop;

import java.util.Arrays;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.remoting.jaxws.SimpleJaxWsServiceExporter;
import org.springframework.transaction.PlatformTransactionManager;

import com.epam.javatraining2016.autoreapirshop.endpoint.AutoRepairShopServiceEndpoint;

@SpringBootApplication
@EnableCaching
@MapperScan("com.epam.javatraining2016.autoreapirshop.dao")
public class Server extends CachingConfigurerSupport {

  @Bean                                                                                                        
  DataSource dataSource(@Value("${jdbc.url}") String url, @Value("${jdbc.user}") String username,              
      @Value("${jdbc.password}") String password) {                                                            
    DriverManagerDataSource retval = new DriverManagerDataSource();                                            
    retval.setDriverClassName("org.postgresql.Driver");                                                        
    retval.setUrl(url);                                                                                        
    retval.setUsername(username);                                                                              
    retval.setPassword(password);                                                                              
    return retval;                                                                                             
  }                                                                                                            

  @Bean
  PlatformTransactionManager dataSourceTransactionManager(DataSource dataSource) {
    return new DataSourceTransactionManager(dataSource);
  }
  
  @Bean
  SqlSessionFactoryBean sqlSessionFactory(DataSource dataSource) {
    SqlSessionFactoryBean retval = new SqlSessionFactoryBean();
    retval.setDataSource(dataSource);
    retval.setConfigLocation( new ClassPathResource("com/epam/javatraining2016/autoreapirshop/dao/mybatis-config.xml") );
    return retval;
  }
 
  @Bean
  AutoRepairShopServiceEndpoint autoRepairShopServiceEndpoint() {
    return new AutoRepairShopServiceEndpoint();
  }

  @Bean
  SimpleJaxWsServiceExporter simpleJaxWsServiceExporter() {
    SimpleJaxWsServiceExporter retval = new SimpleJaxWsServiceExporter();
    retval.setBaseAddress("http://localhost:8080/");
    return retval;
  }

  @Bean                                                                                                     
  @Override                                                                                                 
  public CacheManager cacheManager() {                                                                      
    SimpleCacheManager cacheManager = new SimpleCacheManager();                                             
    Cache[] caches = {new ConcurrentMapCache("OrderStatus"),                                                
        new ConcurrentMapCache("OrderHistoryRecordStatusChange"),                                           
        new ConcurrentMapCache("OrderHistoryComment"), new ConcurrentMapCache("Order"),                     
        new ConcurrentMapCache("Client")};                                                                  
    cacheManager.setCaches(Arrays.asList(caches));                                                          
    return cacheManager;                                                                                    
  }
  
  public static void main(String[] args) throws InterruptedException {
    SpringApplication application = new SpringApplication(Server.class);
    // Нам не нужен встроенный tomcat, SimpleJaxWsServiceExporter использует собственный http сервер
    application.setWebEnvironment(false);
    application.run(args);
  }

}
